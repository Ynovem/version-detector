interface IBaseVersionDetectorConfig {
	type: string;
}
interface IBaseVersionDetector extends IBaseVersionDetectorConfig{}
abstract class BaseVersionDetector implements IBaseVersionDetector{
	type: string;

	protected constructor(config: IBaseVersionDetectorConfig) {
		this.type = config.type;
	}

	abstract detect() : string | null;
}

interface IUrlBasedVersionDetectorConfig extends IBaseVersionDetector{
	url: string;
	parser?: (string) => string | null;
}
interface IUrlBasedVersionDetector extends IBaseVersionDetector{
	parser: (string) => string | null;
}
class UrlBasedVersionDetector extends BaseVersionDetector implements IUrlBasedVersionDetector {
	type: string;
	url: string;
	parser: (string) => (string | null) = (value) => value;

	constructor(config: IUrlBasedVersionDetectorConfig) {
		super(config);

		this.url = config.url;
		if (config.parser) {
			this.parser = config.parser;
		}
	}

	detect(): string | null {
		return null;
	}
}

interface IRegistryBasedVersionDetectorConfig extends IBaseVersionDetector {
	path: string;
	key: string;
	parser?: (string) => string | null;
}
interface IRegistryBasedVersionDetector extends IRegistryBasedVersionDetectorConfig {
	parser: (string) => string | null;
}
class RegistryBasedVersionDetector extends BaseVersionDetector implements  IRegistryBasedVersionDetector {
	key: string;
	path: string;
	parser: (string) => (string | null);

	constructor(config: IRegistryBasedVersionDetectorConfig) {
		super(config);

		this.key = config.key;
		this.path = config.path;
		if (config.parser) {
			this.parser = config.parser;
		} else {
			this.parser = (value) => value;
		}
	}

	detect(): string | null {
		return null;
	}
}

interface IFileBasedVersionDetectorConfig extends IBaseVersionDetector {
	path: string;
	parser?: (string) => string | null;
	locator?: IRegistryBasedVersionDetector | null;
}
interface IFileBasedVersionDetector extends IFileBasedVersionDetectorConfig {
	parser: (string) => string | null;
}
class FileBasedVersionDetector extends BaseVersionDetector implements IFileBasedVersionDetector {
	path: string;
	parser: (string) => (string | null) = (value) => value;
	locator?: (IRegistryBasedVersionDetector | null) = null;

	constructor(config: IFileBasedVersionDetectorConfig) {
		super(config);

		this.path = config.path;
		if (config.parser) {
			this.parser = config.parser;
		}
		if (config.locator) {
			this.locator = new RegistryBasedVersionDetector(config.locator);
		}
	}

	detect(): string | null {
		return null;
	}
}

const detectors: (BaseVersionDetector)[] = [];

detectors.push(new UrlBasedVersionDetector(	{
		type: "url",
		url: "http://localhost:8008/management/stat",
		parser: null
	}));
detectors.push(new UrlBasedVersionDetector(	{
	type: "url",
	url: "http://localhost:8008/management/stat",
	parser: function(resp) {
		if (resp && resp.hasOwnProperty("version")) {
			return resp["version"];
		}

		return null;
	}
}));
detectors.push(new RegistryBasedVersionDetector({
	type: "registry",
		path: "HKEY_LOCAL_MACHINE \\SOFTWARE\\OPSWAT\\Metadefender",
	key: "version",
	parser: null
}));
detectors.push(new RegistryBasedVersionDetector({
	type: "registry",
		path: "HKEY_LOCAL_MACHINE \\SOFTWARE\\OPSWAT\\Metadefender",
	key: "version",
	parser: function(value) {
	const pattern = /\d+\.(\d+)\.\d+/g;

	if (value) {
		const result = value.match(pattern);
		if (result.length) {
			return result[0];
		}
	}

	return null;
}
}));
detectors.push(new FileBasedVersionDetector({
	type: "file",
		path: "C:/Program files (32)/OPSWAT/Metadefender/version.txt",
	parser: null
}));
detectors.push(new FileBasedVersionDetector({
	type: "file",
		path: "C:/Program files (32)/OPSWAT/Metadefender/version.txt",
	parser: function(content) {
	const pattern = /\d+\.(\d+)\.\d+/g;

	if (content) {
		const result = content.match(pattern);
		if (result.length) {
			return result[0];
		}
	}

	return null;
},
	locator: null
}));
detectors.push(new FileBasedVersionDetector({
	type: "file",
		path: "C:/Program files (32)/OPSWAT/Metadefender/version.txt",
	parser: function(content) {
	const pattern = /\d+\.(\d+)\.\d+/g;

	if (content) {
		const result = content.match(pattern);
		if (result.length) {
			return result[0];
		}
	}

	return null;
},
	locator: {
		type: "registry",
			path: "HKEY_LOCAL_MACHINE \\SOFTWARE\\OPSWAT\\Metadefender",
			key: "version",
			parser: null
	}
}));

function detector(detectors: (BaseVersionDetector)[]) {
	for (let i = 0; i<detectors.length; ++i) {
		let possibleVersion = detectors[i].detect();
		if (possibleVersion) {
			return possibleVersion;
		}
	}

	return null;
}